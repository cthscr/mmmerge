local LogId = "CharacterOutfits"
local MF = Merge.Functions
MF.LogInit1(LogId)
local MC, MM, MO = Merge.Consts, Merge.ModSettings, Merge.Offsets

local u2 = mem.u2
local random, sqrt = math.random, math.sqrt

local OldVoiceCount, NewVoiceCount = 30, nil
local ItemSize = 2
local VoiceSetSize = 100
local VoiceTypes = 6

function structs.f.SpcAttack(define)
	define
	.u2 'Spell'
	.u1 'Skill'
	.u1 'DamageType'
	.u2 'SpellPoints'
	.u2 'Recovery'
	.i8 'NextTime'
	.bit('IgnoreByDefault', 0x1)
	.bit('OverwriteMeleeBlaster', 0x2)
	.bit('OverwriteMissileBlaster', 0x4)
	.bit('PreferWand', 0x8)
	.bit('CooldownReducedBySpeed', 0x10)
	.bit('RecoveryReducedBySpeed', 0x20)
	.bit('CooldownReducedBySkill', 0x40)
	.bit('RecoveryReducedBySkill', 0x80)
	.bit('CooldownReducedByHaste', 0x100)
	.bit('RecoveryReducedByHaste', 0x200)
	.bit('CooldownOther', 0x400)
	.bit('UseOtherCooldown', 0x800)
	.u4 'Bits'
	.u4 'Cooldown'
	.i4 'Projectile'
	.i4 'Sound'
end

MC.PlayerBuffs2Count = 7
MC.PlayerBuffsCount = 27 + MC.PlayerBuffs2Count
MC.PlayerDebuffsCount = 20 -- Conditions 0-12,15,17-19

function structs.f.PlayerExtra(define)
	define
	.alt.array(2).struct(structs.SpcAttack) 'SpcAttack'
	.struct(structs.SpcAttack) 'SpcAttackMelee'
	.struct(structs.SpcAttack) 'SpcAttackRanged'
	.array(27, 27 + MC.PlayerBuffs2Count - 1).struct(structs.SpellBuff) 'SpellBuffs2'
	.array(MC.PlayerDebuffsCount).struct(structs.SpellBuff) 'Debuffs'
	.size = 0x40 + 0x10 * (MC.PlayerBuffs2Count + MC.PlayerDebuffsCount)
end

MC.PlayerBuffs2Offset = 0x40
MC.PlayerDebuffsOffset = 0x40 + 0x10 * MC.PlayerBuffs2Count

local player_extra_size = structs.PlayerExtra["?size"]

MO.PlayersExtra = mem.StaticAlloc(50 * player_extra_size)

local OldGame = structs.f.GameStructure
function structs.f.GameStructure(define)
   OldGame(define)
   define
	[0].struct(structs.CharacterVoices)  'CharacterVoices'
	[0x4fcb78].array(1).struct(structs.CharacterPortrait) 'CharacterPortraits'
	[0x4fcb78].array(1).struct(structs.CharacterDollType) 'CharacterDollTypes'
	[0x4FDFF8].array(7).array(19).u1 'StatConditionModifiers'
	[MO.PlayersExtra].array(50).struct(structs.PlayerExtra) 'PlayersExtra'
end

function structs.f.CharacterVoices(define)
	define
	[0x4fcb78].array(0).array(100).u2 'Sounds'
	[0x4fcb78].array(0).array(VoiceTypes).u1 'Avail'
end

function structs.f.CharacterPortrait(define)

	define
	.u1 'DollType'
	.u1 'DefClass'
	.u1 'DefVoice'
	.u1 'DefSex'
	.u1 'AvailableAtStart'
	.i2 'BodyX'
	.i2 'BodyY'
	.EditPChar 'Background'
	.EditPChar 'Body'
	.EditPChar 'Head'
	.EditPChar 'LHd'
	.EditPChar 'LHu'
	.EditPChar 'LHo'
	.EditPChar 'RHb'
	.EditPChar 'unk1'
	.EditPChar 'unk2'
	.EditPChar 'RHd'
	.EditPChar 'RHu'
	.EditPChar 'FacePrefix'
	.u2 'DefAttackM'
	.u2 'DefAttackR'
	.u2	'NPCPic'
	.u1	'Race'
	.i2 'HelmX'
	.i2 'HelmY'

end

function structs.f.CharacterDollType(define)

	define
	.b1 'Bow'
	.b1 'Armor'
	.b1 'Helm'
	.b1 'Belt'
	.b1 'Boots'
	.b1 'Cloak'
	.b1 'Weapon'
	.i2 'RHoX'
	.i2 'RHoY'
	.i2 'RHcX'
	.i2 'RHcY'
	.i2 'RHfX'
	.i2 'RHfY'
	.i2 'LHoX'
	.i2 'LHoY'
	.i2 'LHcX'
	.i2 'LHcY'
	.i2 'LHfX'
	.i2 'LHfY'
	.i2 'OHOffsetX'
	.i2 'OHOffsetY'
	.i2 'MHOffsetX'
	.i2 'MHOffsetY'
	.i2 'BowX'
	.i2 'BowY'
	.i2 'ShieldX'
	.i2 'ShieldY'

end

function GetRace(Player, PlayerIndex)
	return Game.CharacterPortraits[Player.Face].Race
end

local function GetPlayer(ptr)
	local PlayerId = (ptr - Party.PlayersArray["?ptr"])/Party.PlayersArray[0]["?size"]
	return Party.PlayersArray[PlayerId], PlayerId
end

local function GetMon(ptr)
	local MapMonP = Map.Monsters["?ptr"]
	if ptr > MapMonP then
		local i = (ptr-MapMonP)/Map.Monsters[0]["?size"]
		return Map.Monsters[i], i
	end
end

-- ecx - player ptr
MO.GetPlayerExtraPtr = mem.asmproc([[
movsx eax, word ptr [ecx + 0x1BF0]
sub eax, 400
imul eax, ]] .. player_extra_size .. [[;
add eax, ]] .. MO.PlayersExtra .. [[;
retn
]])

-- ecx - player ptr, edx - debuff index
MO.GetPlayerBuffPtr = mem.asmproc([[
push esi
push edi
mov esi, ecx
mov edi, edx
cmp edi, 27
jge @buffs2
mov eax, esi
add eax, 0x1A34
mov ecx, edi
jmp @common
@buffs2:
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
add eax, ]] .. MC.PlayerBuffs2Offset .. [[;
mov ecx, edi
sub ecx, 27
@common:
imul ecx, 0x10
add eax, ecx
pop edi
pop esi
retn
]])

-- ecx - player ptr, edx - debuff index
MO.GetPlayerDebuffPtr = mem.asmproc([[
push esi
push edi
mov esi, ecx
mov edi, edx
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
add eax, ]] .. MC.PlayerDebuffsOffset .. [[;
mov ecx, edi
imul ecx, 0x10
add eax, ecx
pop edi
pop esi
retn
]])


-- ecx - player ptr, edx - condition index
MO.CheckConditionDebuff = mem.asmproc([[
push ebx
xor ebx, ebx
mov eax, dword ptr [ecx + edx * 8]
or eax, dword ptr [ecx + edx * 8 + 4]
jz @debuff
inc ebx
@debuff:
call absolute ]] .. MO.GetPlayerDebuffPtr .. [[;
mov ecx, eax
xor eax, eax
cmp dword ptr [ecx + 4], eax
jl @end
jg @pos
cmp dword ptr [ecx], eax
jbe @end
@pos:
inc eax
inc eax
@end:
add eax, ebx
pop ebx
retn
]])

-- ecx - player ptr
MO.ClearExpiredBuffs2 = mem.asmproc([[
push esi
push edi
push ebx
mov esi, dword ptr [0xB20EC0]
mov edi, dword ptr [0xB20EBC]
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
mov ecx, eax
mov ebx, ]] .. MC.PlayerBuffs2Count .. [[;
test ebx, ebx
jz @end
add ecx, ]] .. MC.PlayerBuffs2Offset .. [[;
@loop:
mov eax, dword ptr [ecx + 4]
or eax, dword ptr [ecx]
jz @next
cmp dword ptr [ecx + 4], esi
jg @next
jl @clear
cmp dword ptr [ecx], edi
ja @next
@clear:
xor eax, eax
mov dword ptr [ecx], eax
mov dword ptr [ecx + 4], eax
mov dword ptr [ecx + 8], eax
mov dword ptr [ecx + 0xC], eax
@next:
add ecx, 0x10
dec ebx
jnz @loop
@end:
pop ebx
pop edi
pop esi
retn
]])

-- ecx - player ptr
MO.ClearExpiredDebuffs = mem.asmproc([[
push esi
push edi
push ebx
mov esi, dword ptr [0xB20EC0]
mov edi, dword ptr [0xB20EBC]
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
mov ecx, eax
mov ebx, ]] .. MC.PlayerDebuffsCount .. [[;
test ebx, ebx
jz @end
add ecx, ]] .. MC.PlayerDebuffsOffset .. [[;
@loop:
mov eax, dword ptr [ecx + 4]
or eax, dword ptr [ecx]
jz @next
cmp dword ptr [ecx + 4], esi
jg @next
jl @clear
cmp dword ptr [ecx], edi
ja @next
@clear:
xor eax, eax
mov dword ptr [ecx], eax
mov dword ptr [ecx + 4], eax
mov dword ptr [ecx + 8], eax
mov dword ptr [ecx + 0xC], eax
@next:
add ecx, 0x10
dec ebx
jnz @loop
@end:
pop ebx
pop edi
pop esi
retn
]])

-- ecx - player ptr
MO.ClearExpiredBuffs2Debuffs = mem.asmproc([[
push esi
push edi
push ebx
mov esi, dword ptr [0xB20EC0]
mov edi, dword ptr [0xB20EBC]
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
mov ecx, eax
mov ebx, ]] .. (MC.PlayerBuffs2Count + MC.PlayerDebuffsCount) .. [[;
test ebx, ebx
jz @end
add ecx, ]] .. MC.PlayerBuffs2Offset .. [[;
@loop:
mov eax, dword ptr [ecx + 4]
or eax, dword ptr [ecx]
jz @next
cmp dword ptr [ecx + 4], esi
jg @next
jl @clear
cmp dword ptr [ecx], edi
ja @next
@clear:
xor eax, eax
mov dword ptr [ecx], eax
mov dword ptr [ecx + 4], eax
mov dword ptr [ecx + 8], eax
mov dword ptr [ecx + 0xC], eax
@next:
add ecx, 0x10
dec ebx
jnz @loop
@end:
pop ebx
pop edi
pop esi
retn
]])

-- Condition colorer
mem.asmpatch(0x416FF3, [[
cmp ebx, 0x13
ja absolute 0x417011
cmp ebx, 0x10
ja absolute 0x417006
]])

-- Condition name
MO.GetConditionName = mem.asmproc([[
cmp ecx, 0x14
jae @good
mov eax, dword ptr [ecx*4 + 0xBB3010]
retn
@good:
mov eax, dword ptr [0x6015D0]
retn
]])

mem.asmpatch(0x4178DD, [[
push eax
mov ecx, edi
call absolute ]] .. MO.GetConditionName .. [[;
pop ecx
push eax
mov eax, ecx
]])
mem.asmpatch(0x41796E, "cmp dword ptr [ebp - 0x8], 0x4FDFF8")
mem.asmpatch(0x4181A8, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
mov eax, ecx
]])
mem.asmpatch(0x41AAD3, [[
mov ecx, ebp
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x41CADC, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
mov eax, ecx
]])
mem.asmpatch(0x4304E7, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x43260F, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x432790, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x4669AB, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x466A11, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
mem.asmpatch(0x4C915F, [[
mov ecx, eax
call absolute ]] .. MO.GetConditionName .. [[;
push eax
]])
-- Load condition names for Unarmed and Feebleminded
mem.asmpatch(0x45073E, [[
mov ecx, dword ptr [0x60157C]
mov dword ptr [0xBB305C], ecx
mov ecx, dword ptr [0x601D90]
]])
mem.asmpatch(0x48BCC8, [[
mov eax, dword ptr [0x60157C]
mov dword ptr [0xBB305C], eax
mov eax, dword ptr [0x601D90]
]])

-- ecx - player ptr
MO.GetMainCondDebuff = mem.asmproc([[
push esi
push edi
push ebx
mov esi, ecx
xor edi, edi
@loop:
mov ebx, dword ptr [edi*4 + 0x4FDFA8]
mov eax, dword ptr [esi + ebx*8]
or eax, dword ptr [esi + ebx*8 + 4]
;test eax, eax
jnz @end
cmp ebx, 0xD
je @next
cmp ebx, 0xE
je @next
cmp ebx, 0x10
je @next
mov edx, ebx
mov ecx, esi
call absolute ]] .. MO.GetPlayerDebuffPtr .. [[;
mov ecx, eax
call absolute 0x42D70E
test eax, eax
jnz @end
@next:
inc edi
cmp edi, 0x14
jl @loop
mov ebx, edi
@end:
mov eax, ebx
pop ebx
pop edi
pop esi
retn
]])

-- ecx - player ptr, edx - spell, arg0 - source
--   Source:
--     1 - player spell book
--     2 - quick spell
--     3 - extra quick spells
--     4 - spell scroll
MO.CanPlayerCastSpell = mem.asmproc([[
push ebx
mov ebx, dword ptr [esp + 8]
push esi
push edi
mov esi, ecx
mov edi, edx
push 0x12
pop edx
call absolute ]] .. MO.CheckConditionDebuff .. [[;
nop
nop
nop
nop
nop
pop edi
pop esi
pop ebx
retn 4
]])

mem.hook(MO.CanPlayerCastSpell + 0x13, function(d)
	local t = {PlayerPtr = d.esi, Spell = d.edi, Check = d.eax,
		Source = d.ebx, Result = (d.eax == 0) and true or false}
	if t.Source == 4 and t.Result == false then
		t.Result = MF.SettingEqNum(MM.FeeblemindedDisableScrolls, 0)
	end
	events.call("CanPlayerCastSpell", t)
	d.eax = t.Result and 1 or 0
end)

-- Feebleminded condition/debuff
--   Quick Spell key pressed
mem.asmpatch(0x42E846, [[
movzx ebx, byte ptr [edi + 0x1C45]
test ebx, ebx
jz absolute 0x42E923
mov ecx, edi
mov edx, ebx
push 2
call absolute ]] .. MO.CanPlayerCastSpell .. [[;
test eax, eax
jz absolute 0x42E923
]])
--   Action 142 (Cast Spell)
mem.asmpatch(0x4320B7, [[
jz absolute 0x43036D
mov ecx, 0xB20E90
push dword ptr [esp + 0x20]
call absolute 0x4026F4
mov ecx, eax
mov edx, dword ptr [esp + 0x10]
push 1
call absolute ]] .. MO.CanPlayerCastSpell .. [[;
test eax, eax
jz absolute 0x43036D
]])
--   Action 146 (Use SpellScroll)
mem.asmpatch(0x4320C9, [[
jz absolute 0x43036D
mov ecx, 0xB20E90
push dword ptr [esp + 0x20]
call absolute 0x4026F4
mov ecx, eax
mov edx, dword ptr [esp + 0x10]
push 4
call absolute ]] .. MO.CanPlayerCastSpell .. [[;
test eax, eax
jz absolute 0x43036D
]])

-- Unarmed condition/debuff
--   zero phys damage to monster (like Mistform buff)
mem.asmpatch(0x437260, [[
push eax
mov ecx, eax
mov edx, 0x13
call absolute ]] .. MO.CheckConditionDebuff .. [[;
test eax, eax
pop eax
jg absolute 0x437268
cmp dword [eax + 0x1BD4], ebx
]])

-- Get main condition stat modifier
if MF.SettingGtNum(MM.AllConditionsModifiers, 0) then
	-- Use minimal stat modifier of all current conditions
	mem.asmpatch(0x48E14E, [[
	push esi
	push edi
	mov esi, ecx
	mov edi, dword ptr [esp + 0xC]
	push ebx
	imul edi, 0x13
	add edi, 0x4FDFF8
	mov ebx, 0x64
	xor ecx, ecx
	@loop:
	mov eax, dword ptr [esi+ecx*8]
	or eax, dword ptr [esi+ecx*8+4]
	test eax, eax
	jnz @f1
	; check debuff
	mov edx, ecx
	push ecx
	mov ecx, esi
	call absolute ]] .. MO.GetPlayerDebuffPtr .. [[;
	mov ecx, eax
	call absolute 0x42D70E
	pop ecx
	test eax, eax
	jz @next
	@f1:
	movzx eax, byte ptr [edi+ecx]
	cmp eax, 0x64
	je @next
	jl @lower
	cmp ebx, 0x64
	je @set
	@lower:
	cmp ebx, eax
	jle @next
	@set:
	mov ebx, eax
	@next:
	inc ecx
	cmp ecx, 0x12
	jl @loop
	mov eax, ebx
	pop ebx
	pop edi
	pop esi
	retn 4
	]])
else
	-- Apply main condition modifiers only
	mem.asmpatch(0x48E14E, [[
	call absolute 0x48E127
	cmp eax, 0x12
	jbe @end
	mov eax, 0x12
	@end:
	]])
end
--
mem.asmpatch(0x48E13B, "cmp edx, 0x14")
mem.asmpatch(0x48E140, "push 0x14")

mem.asmpatch(0x44775E, "cmp edi, 0x14")
mem.asmpatch(0x48FB8D, "cmp eax, 0x14")
mem.asmpatch(0x4B57C5, "cmp eax, 0x14")

-- Check condition and debuff for face expressions
mem.asmpatch(0x48FB88, "call absolute " .. MO.GetMainCondDebuff)

-- Face expressions for Feebleminded and Unarmed
mem.asmpatch(0x48FBF1, [[
jbe @end
cmp eax, 0x12
jne @f1
mov eax, 0x1A
jmp @f2
@f1:
cmp eax, 0x13
jne @f3
mov eax, 0x1C
@f2:
mov word ptr [esi + 0x1C86], ax
@f3:
jmp absolute 0x48FE3D
@end:
]])

-- Clear all 26 player buffs after the rest (rather than 20)
if MF.SettingGtNum(MM.ClearAllBuffsAfterRest, 0) then
	mem.asmpatch(0x48FEDE, "push 0x1B")
end
-- Clear SpellBuffs2 and expired debuffs after the rest
mem.asmpatch(0x48FEF6, [[
call absolute ]] .. MO.GetPlayerExtraPtr .. [[;
add eax, ]] .. MC.PlayerBuffs2Offset .. [[;
mov edi, eax
mov ebp, ]] .. MC.PlayerBuffs2Count .. [[;
test ebp, ebp
jz @end
@loop:
mov ecx, edi
call absolute 0x455E3C
add edi, 0x10
dec ebp
jnz @loop
@end:
mov ecx, esi
call absolute ]] .. MO.ClearExpiredDebuffs .. [[;
mov ecx, esi
call absolute 0x48F9B2
]])
-- Clear conditions 18-19, debuffs 1-4,18-19
mem.asmpatch(0x48FF25, [[
mov ebp, 4
@loop1:
mov ecx, esi
mov edx, ebp
call absolute ]] .. MO.GetPlayerDebuffPtr .. [[;
mov ecx, eax
call absolute 0x455E3C
dec ebp
jnz @loop1
mov ebp, 2
@loop2:
mov ecx, esi
mov edx, ebp
add edx, 0x11
call absolute ]] .. MO.GetPlayerDebuffPtr .. [[;
mov ecx, eax
call absolute 0x455E3C
dec ebp
jnz @loop2
mov ecx, esi
mov dword ptr [esi + 0x90], ebx
mov dword ptr [esi + 0x94], ebx
mov dword ptr [esi + 0x98], ebx
mov dword ptr [esi + 0x9C], ebx
mov dword ptr [esi + 0x68], ebx
]])

-- Clear everything _after_ the rest
mem.asmpatch(0x41EB33, [[
mov ecx, 0xB20E90
call absolute 0x48FE9C
mov eax, dword ptr [0x51E330]
]])
-- Don't clear everything before the rest
-- TODO: clear buffs still?
mem.nop(0x431B07, 5)

-- Clear expired SpellBuffs2 and debuffs
mem.asmpatch(0x49292B, [[
mov ecx, esi
call absolute ]] .. MO.ClearExpiredBuffs2Debuffs .. [[;
lea edi, [esi+0x1A34]
]])

------------------
-- Character voice

local function ProcessVoicesTable()

	local function GenerateVoicesTable()

		local StartIndex = 5000

		local RowHeaders = 		{"Clear1", "Clear2", "Closed1", "Closed2", "Oops1", "Oops2", "Ready1", "Ready2", "BadItem1", "BadItem2",
								"GoodItem1", "GoodItem2", "CantIdent1", "CantIdent2", "Repaired1", "Repaired2", "CantRep1", "CantRep2", "EasyFight1", "EasyFight2",
								"HardFight1", "HardFight2", "CantIdMon1", "CantIdMon2", "Ha!1", "Ha!2", "Hungry1", "Hungry2", "Ouch1", "Ouch2",
								"Injured1", "Injured2", "HardInjured1", "HardInjured2", "Mad1", "Mad2", "Mad3", "Mad4", "Misc1", "Misc2",
								"Misc3", "Misc4", "Misc5", "Misc6", "CantRestHere1", "CantRestHere2", "NeedMoreGold1", "NeedMoreGold2", "InventoryFull1", "InventoryFull2",
								"Yes!1", "Yes!2", "Nah1", "Nah2", "ClosedB1", "ClosedB2", "LearnSpell1", "LearnSpell2", "CantLearn1", "CantLearn2",
								"No1", "No2", "Hello1", "Hello2", "Hello3", "Hello4", "Hello5", "Hello6", "n/u", "n/u",
								"n/u", "n/u", "Win!1", "Win!2",	"Heh1", "Heh2", "LastStanding1", "LastStanding2", "HardFightEnd", "Enter1",
								"Enter2", "Enter3", "Yes1", "Yes2",	"Thanks1", "Thanks2", "GoodFight1", "GoodFight2","n/u", "n/u",
								"n/u", "n/u", "Move!1", "Move!2", "n/u", "n/u", "n/u", "n/u", "n/u", "n/u"}


		local VoiceTxtTable = io.open("Data/Tables/Character voices.txt", "w")

		VoiceTxtTable:write("Sound type/Voice set id\9")
		for i = 0, OldVoiceCount - 1 do
			VoiceTxtTable:write(i .. "\9")
		end
		VoiceTxtTable:write("\n")

		for iR = 0, VoiceSetSize-1 do

			local CurStr = RowHeaders[iR+1]

			for iL = 0, OldVoiceCount-1 do

				CurStr = CurStr .. "\9" .. iR + iL*VoiceSetSize + StartIndex

			end

			VoiceTxtTable:write(CurStr .. "\n")

		end

		---- Availability table

		VoiceTxtTable:write("\n")
		VoiceTxtTable:write("Man\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9x\9x\9x\9-\9-\9-\9-\9-\9-\n")
		VoiceTxtTable:write("Woman\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9x\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\n")
		VoiceTxtTable:write("Dragon\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9-\9x\9-\9-\9-\9-\9-\n")

		io.close(VoiceTxtTable)

	end

	local VoiceTxtTable = io.open("Data/Tables/Character voices.txt", "r")

	if not VoiceTxtTable then
		GenerateVoicesTable()
		VoiceTxtTable = io.open("Data/Tables/Character voices.txt", "r")
	end

	---- Load table

	local LineIt = VoiceTxtTable:lines()
	local Header = string.split(LineIt(), "\9")
	local NewVoiceCount

	NewVoiceCount = tonumber(table.remove(Header))
	while table.maxn(Header) > 0 and not NewVoiceCount do
		NewVoiceCount = tonumber(table.remove(Header))
	end

	if not NewVoiceCount then
		return
	end

	local VoiceTableSize = VoiceSetSize*(NewVoiceCount+1)*ItemSize
	local VoiceTablePtr = mem.StaticAlloc(VoiceTableSize + (NewVoiceCount+1)*VoiceTypes)
	local CurSoundType = 0

	-- Voice sets

	for i = 1, VoiceSetSize do

		local line = LineIt()
		local RowSet = string.split(line, "\9")

		for i = 0, NewVoiceCount do
			mem["u".. ItemSize][VoiceTablePtr + CurSoundType*ItemSize + VoiceSetSize*i*ItemSize] = tonumber(RowSet[i+2])
		end

		CurSoundType = CurSoundType + 1

	end

	-- Voice availability

	CurSoundType = 0
	for line in LineIt do
		local RowSet = string.split(line, "\9")
		local flag

		if table.maxn(RowSet) > NewVoiceCount and RowSet[1] ~= "" and RowSet[1] ~= "Notes" then
			for i = 0, NewVoiceCount do
				mem.u1[VoiceTablePtr+VoiceTableSize+i*VoiceTypes+CurSoundType] = RowSet[i+2] == "x" and 1 or 0
			end
			CurSoundType = CurSoundType + 1
		end

	end

	----

	local function ChangeCharacterVoicesArray(name, p, count)
		structs.o.CharacterVoices[name] = p
		internal.SetArrayUpval(Game.CharacterVoices[name], "o", p)
		internal.SetArrayUpval(Game.CharacterVoices[name], "count", count)
	end

	ChangeCharacterVoicesArray("Sounds", VoiceTablePtr, NewVoiceCount+1)
	ChangeCharacterVoicesArray("Avail", VoiceTablePtr+VoiceTableSize, NewVoiceCount+1)

 	-- table handler

	local function VoiceAvailable(Voice, Player)

		local Sex	= Player:GetSex()
		local Class	= Player.Class
		local Portrait = Game.CharacterPortraits[Player.Face]

		if Voice < 0 or Voice >= Game.CharacterVoices.Avail.count then
			Player.Voice = Portrait.DefVoice
			return false
		end

		if Player.Face == 67 or Player.Face == 68 then
			return Game.CharacterVoices.Avail[Voice][5]
		elseif Portrait.Race == const.Race.Dragon then
			return Game.CharacterVoices.Avail[Voice][2]
		elseif Game.Races[Portrait.Race].Kind == const.RaceKind.Undead
				and Game.Races[Portrait.Race].Family ~= const.RaceKind.Vampire then
			return Game.CharacterVoices.Avail[Voice][Sex + 3]
		else
			return Game.CharacterVoices.Avail[Voice][Sex]
		end

	end

	-- Set def voice
	mem.hook(0x433a8e, function(d)
		local Player = GetPlayer(d.ebp)
		Player.Voice = Game.CharacterPortraits[Player.Face].DefVoice
	end)

	mem.nop(0x43379c, 2)
	mem.asmpatch(0x4337a5, "cmp eax, 1")
	mem.hook(0x4337a0, function(d)
		d.eax = VoiceAvailable(mem.i4[d.ecx+0x1be4], Party.Players[0])
		d.ecx = d.eax
	end)

	mem.nop(0x4337f3, 2)
	mem.asmpatch(0x4337fc, "cmp eax, 1")
	mem.hook(0x4337f7, function(d)
		d.eax = VoiceAvailable(mem.i4[d.ecx+0x1be4], Party.Players[0])
		d.ecx = d.eax
	end)

	mem.asmpatch(0x492c6f, "xor edi, edi")
	mem.asmpatch(0x492c71, "inc edi")

	local CurPtr = mem.asmpatch(0x492c8c, [[
	mov eax, dword [ds:ebp-0x8]
	mov eax, dword [ds:eax+0x1be4]
	imul eax, eax, ]] .. VoiceSetSize .. [[;
	lea eax, dword [ds:eax+esi*2]
	lea eax, dword [ds:eax-2]
	imul eax, eax, ]] .. ItemSize .. [[;
	nop
	nop
	nop
	nop
	nop
	movzx esi, word [ds:eax+]] .. VoiceTablePtr .. [[];]])

	mem.hook(CurPtr + 19 + (VoiceSetSize < 128 and 3 or 6), function(d)
		if d.edx < 2 then
			d.edx = random(0,1)
			if u2[VoiceTablePtr + d.eax + d.edx * ItemSize] == 0 then
				d.edx = 1 - d.edx
			end
		end
		d.eax = d.eax + d.edx * ItemSize
	end)

	mem.asmpatch(0x490a71, [[
	dec ecx
	imul ecx, ecx, ]] .. ItemSize .. [[;
	movzx ecx, word [ds:ecx*2+]] .. VoiceTablePtr .. [[];]])

	mem.IgnoreProtection(true)
	mem.u1[0x43378d+6] = NewVoiceCount
	mem.u1[0x4337e9+6] = NewVoiceCount
	mem.u1[0x490a64 + 2] = VoiceSetSize/2
	mem.IgnoreProtection(false)


end


local function ChangeGameArray(name, p, count)
	structs.o.GameStructure[name] = p
	internal.SetArrayUpval(Game[name], "o", p)
	internal.SetArrayUpval(Game[name], "count", count)
end

----------- Portraits

local function SetChooseCharHook()
	mem.autohook2(0x48e0fa, function(d)
		events.call("CharacterChosen")
	end)
end
local function ProcessPortraitsTable()

	local function GeneratePortraitsTable()

		local OldPicCount = 30
		local OldPicsPtrBlocks 	= 	{0x4fcb00, 0x4fcdd0, 0x4fcd58, 0x4fcbf0, 0x4fcc68, 0x4fcce0, 0x4fce48, 0x4fcfb0, 0x4fd028, 0x4fcec0, 0x4fcf38, 0x4fcb78}
		local DefaultBodyTypes 	= 	{0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,2,2,3,3,4,4,0,1,0,0,0,0}
		local DefaultClasses 	=	{4,4,4,4,2,2,2,2,0,0,0,0,12,12,12,12,10,10,10,10,8,8,6,6,14,14,1,1,0,0,0}
		local DefaultSex		= 	{0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0}
		local DefaultAttack		= 	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,137,137,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
		local DefaultAvFlags	=	{"x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","x","-","-","-","-","-","-","-"}
		local DefaultRaces		=	{0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,4,4,5,5,0,0,0,0,0,0}

		local PicsTxtTable = io.open("Data/Tables/Character portraits.txt", "w")
		PicsTxtTable:write("#	Paperdoll type	Def class	Def voice	Def sex	Available at start	Body X	Body Y	Helm X	Helm Y	Background	bod	head	LHd	LHu	LHo	RHb	Unk	Unk	RHd	RHu	Face pics prefix	Def Attack (melee)	Def Attack (range)	NPCPic	Race\n")

		for iR = 0, OldPicCount - 1 do

			local CurStr = 		iR .. "\9" .. DefaultBodyTypes[iR+1] .. "\9" .. DefaultClasses[iR+1] .. "\9" .. iR .. "\9" .. DefaultSex[iR+1] .. "\9"
								   .. DefaultAvFlags[iR+1] .. "\9" .. mem.i4[0x4f6158+iR*8] .. "\9" .. mem.i4[0x4f615c+iR*8] .. "\9" .. "0" .. "\9" .. "0" .. "\9"

			for i, v in ipairs(OldPicsPtrBlocks) do
				CurStr = CurStr .. mem.string(mem.u4[v+iR*4]) .. "\9"
			end

			CurStr = CurStr .. DefaultAttack[iR+1] .. "\9" .. DefaultAttack[iR+1] .. "\9" .. iR + 2901 .. "\9" .. DefaultRaces[iR+1] .. "\n"

			PicsTxtTable:write(CurStr)

		end

		io.close(PicsTxtTable)

	end

	local PicsTxtTable = io.open("Data/Tables/Character portraits.txt", "r")
	local PortraitsTablePtr, PictureNamesPtr
	local Counter, LineSize, PortraitsCount = 0, 68, 0
	local Words, LineIt

	if not PicsTxtTable then
		GeneratePortraitsTable()
		PicsTxtTable = io.open("Data/Tables/Character portraits.txt", "r")
	end

	LineIt = PicsTxtTable:lines()

	LineIt() -- skip header

	for line in LineIt do
		PortraitsCount = PortraitsCount + 1
	end

	PortraitsTablePtr = mem.StaticAlloc(PortraitsCount*LineSize + 0x4 + PortraitsCount*12*12)
	PictureNamesPtr = PortraitsTablePtr + PortraitsCount*LineSize + 0x4

	PicsTxtTable:seek("set")
	LineIt() -- skip header

	Counter = 0
	for line in LineIt do

 		Words = string.split(line, "\9")

		for i = 2, 5 do
			mem.u1[PortraitsTablePtr + i-2 + Counter*LineSize] = tonumber(Words[i]) or 0
		end

		if Words[6] == "x" then
			mem.u1[PortraitsTablePtr + 4 + Counter*LineSize] = 1
		end

		mem.i2[PortraitsTablePtr + 5 + Counter*LineSize] = tonumber(Words[7]) or 0
		mem.i2[PortraitsTablePtr + 7 + Counter*LineSize] = tonumber(Words[8]) or 0

		mem.i2[PortraitsTablePtr + LineSize - 4 + Counter*LineSize] = tonumber(Words[9]) or 0
		mem.i2[PortraitsTablePtr + LineSize - 2 + Counter*LineSize] = tonumber(Words[10]) or 0

		for i = 0, 11 do

			local Word = Words[11 + i]
			local NamePtr = PictureNamesPtr + i*12 + Counter*12*12
			local TPtr = PortraitsTablePtr + 9 + i*4 + Counter*LineSize

			mem.u4[TPtr] = NamePtr

			for iL = 1, string.len(Word) do
				mem.u1[NamePtr + (iL-1)] = string.byte(Word, iL)
			end

		end

		mem.u2[PortraitsTablePtr + 57 + Counter*LineSize] = tonumber(Words[23]) or 0
		mem.u2[PortraitsTablePtr + 59 + Counter*LineSize] = tonumber(Words[24]) or 0
		mem.u2[PortraitsTablePtr + 61 + Counter*LineSize] = tonumber(Words[25]) or 0
		mem.u1[PortraitsTablePtr + 63 + Counter*LineSize] = tonumber(Words[26]) or 0

		Counter = Counter + 1
	end

	ChangeGameArray("CharacterPortraits", PortraitsTablePtr, PortraitsCount)

	---- Pictures

	-- Player:GetSex() correction

	mem.hook(0x48f5e5, function(d)

		Game.CurrentCharPortrait = d.ecx
		d.eax = Game.CharacterPortraits[d.ecx].DefSex
		d.ecx = d.eax

	end)
	mem.asmpatch(0x48f5ea, "jmp 0x48f5fb - 0x48f5ea")

	--

	local TmpTable = 	{{0x439971, 0x4c4eee},				-- Background
						{0x43998f, 0x4c4f17}, 				-- Body
						{0x4399ad}, 						-- Head
						{0x4399cb, 0x4c5021, 0x4c5021}, 	-- LHd
						{0x4399e9}, 						-- LHu
						{0x439a07}, 						-- LHo
						{0x439a25}, 						-- RHb
						{0x439a43}, 						-- Unk
						{0x439a61}, 						-- Unk
						{0x439a7f, 0x4c5094, 0x4c5094}, 	-- RHd
						{0x439a9d}}							-- RHu

	for iR,vR in ipairs(TmpTable) do
		for i, v in ipairs(vR) do
			mem.asmpatch(v, [[
			imul ecx, eax, ]] .. LineSize .. [[;
			add ecx, ]] .. 0x5 + iR*4 .. [[;
			push dword [ds:ecx+]] .. PortraitsTablePtr .. [[];]])
		end
	end

 	-- face
	mem.asmpatch(0x4908f1, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, 0x35
	push dword [ds:eax+]] .. PortraitsTablePtr .. [[];]])

	mem.asmpatch(0x4909c3, [[
	imul ebx, edx, ]] .. LineSize .. [[;
	add ebx, 0x35
	lea ebp, dword [ds:ebx+]] .. PortraitsTablePtr .. [[];]])

	-- jump over old face pictures loading
	mem.asmpatch(0x4c5563, [[jmp absolute 0x4c55a6]])

	-- load them on the fly
	mem.asmpatch(0x4c4e91, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, 0x35
	push dword [ds:eax+]] .. PortraitsTablePtr .. [[]
	push 0x4fedb4
	push 0x5df0e0
	call absolute 0x4d9f10

	add esp, 0xc

	push 0
	push 0
	push 2
	push 0x5df0e0
	mov ecx, 0x70d3e8
	call absolute 0x410d70

	lea eax, dword [ds:eax+eax*8]
	lea eax, dword [ds:eax*8+0x70d624]
	mov dword [ds:0x518e40], eax

	push dword [ds:0x518e40];]])

 	---- Complexion

	mem.asmpatch(0x43a42c, [[
	movzx eax, al
	imul ecx, eax, ]] .. LineSize .. [[;
	movzx ecx, byte [ds:ecx+]] .. PortraitsTablePtr .. [[];
	mov dword [ss:esp+0x24], ecx
	jmp absolute 0x43a46b]])

	mem.asmpatch(0x4c4f5c, [[
	movzx eax, al
	imul ecx, eax, ]] .. LineSize .. [[;
	movzx ecx, byte [ds:ecx+]] .. PortraitsTablePtr .. [[];
	mov dword [ss:ebp-0x8], ecx
	jmp absolute 0x4c4f9a]])

 	---- Character selection

	mem.nop(0x433859, 5)
	mem.nop(0x433864, 2)
	mem.asmpatch(0x43385e, [[
	@rep:
	cmp byte [ds:eax], ]] .. PortraitsCount - 1 .. [[;
	jge @cle

	inc byte [ds:eax]
	jmp @nex

	@cle:
	mov byte [ds:eax], bl

	@nex:
	movzx ecx, byte [ds:eax]
	imul ecx, ecx, ]] .. LineSize .. [[;
	add ecx, 0x4
	cmp byte [ds:ecx+]] .. PortraitsTablePtr .. [[], 1
	jnz @rep]])

	mem.nop(0x433924, 2)
	mem.asmpatch(0x433926, [[
	jmp @rep
	@cle:
	mov byte [ds:eax], ]] .. PortraitsCount .. [[;

	@rep:
	dec byte [ds:eax]
	js @cle

	movzx ecx, byte [ds:eax]
	imul ecx, ecx, ]] .. LineSize .. [[;
	add ecx, 0x4
	cmp byte [ds:ecx+]] .. PortraitsTablePtr .. [[], 1
	jnz @rep]])

	mem.asmpatch(0x433930, [[
	mov ecx, eax
	imul ecx, ecx, ]] .. LineSize .. [[;
	add ecx, 0x2
	movzx ecx, byte [ds:ecx+]] .. PortraitsTablePtr .. [[]
	mov dword [ss:ebp+0x1be4], ecx]])

	-- Class
	mem.asmpatch(0x433936, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, 0x1
	movzx eax, byte [ds:eax+]] .. PortraitsTablePtr .. [[];]])

	-- Default Character
	mem.asmpatch(0x490479, [[
	@rep:
	imul ecx, eax, ]] .. LineSize .. [[;
	add ecx, ]] .. PortraitsTablePtr .. [[;
	cmp byte [ds:ecx+0x4], 0x1;
	je @equ
	inc eax
	jmp @rep

	@equ:
	mov byte [ds:esi+0x353], al
	mov al, byte [ds:ecx+1]
	mov byte [ds:esi+0x352], al
	movzx eax, byte [ds:ecx+2]
	mov dword [ds:esi+0x1be4], eax
	jmp absolute 0x490498]])

	-- Body coordinates
	mem.nop(0x43a885, 3)
	mem.asmpatch(0x43a888, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. PortraitsTablePtr .. [[;
	movsx ecx, word [ds:eax+0x5]
	movsx eax, word [ds:eax+0x7]
	jmp absolute 0x43a894]])

	mem.nop(0x4c4fa5, 3)
	mem.asmpatch(0x4c4fb9, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. PortraitsTablePtr .. [[;
	movsx ecx, word [ds:eax+0x7]
	movsx eax, word [ds:eax+0x5]
	jmp absolute 0x4c4fc5]])

	-- NPC pictures in Adventurer's Inn
	mem.u4[0x501daf] = 0x64343025 -- %04d
	mem.u1[0x501db3] = 0
	mem.nop(0x4c82eb, 1)
	mem.asmpatch(0x4c82e4, [[
	movsx eax, byte [ds:eax+0x353]
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. PortraitsTablePtr .. [[;
	movsx eax, word [ds:eax+0x3d];]])

	-- Custom attacks
	local function get_portrait_spc_attack(portrait, ranged)
		local addr = PortraitsTablePtr + portrait * LineSize + (ranged and 59 or 57)
		return mem.u2[addr]
	end
	MF.GetPortraitSpcAttack = get_portrait_spc_attack

	local GetCAttack = mem.asmproc([[
	; ptr to character structure in ecx

	movzx eax, byte [ds:ecx+0x353]
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, 0x3b
	cmp word [ds:eax+]] .. PortraitsTablePtr .. [[], 0
	jnz @end
	sub eax, 0x2
	@end:
	movzx eax, word [ds:eax+]] .. PortraitsTablePtr .. [[];
	retn]])

	MO.AttackDistance = mem.StaticAlloc(16)
	MO.AttackIsRanged = MO.AttackDistance + 4
	MO.AttackSpcPtr = MO.AttackDistance + 8

	-- ecx - player_ptr
	-- edx - is_ranged (0 if melee)
	local get_spc_attack_ptr = mem.asmproc([[
	movsx eax, word ptr [ecx+0x1BF0]
	sub eax, 0x190
	imul eax, ]] .. player_extra_size .. [[;
	add eax, ]] .. MO.PlayersExtra .. [[;
	test edx, edx
	jz @end
	add eax, ]] .. structs.SpcAttack["?size"] .. [[;
	@end:
	retn]])
	--MO.GetSpcAttackPtr = get_spc_attack_ptr

	-- ecx - player_ptr
	-- edx - is_ranged (0 if melee)
	local check_spc_attack = mem.asmproc([[
	call absolute ]] .. get_spc_attack_ptr .. [[;
	mov edx, eax
	mov dword ptr []] .. MO.AttackSpcPtr .. [[], eax
	movzx eax, word ptr [edx]
	test eax, eax
	jz @no
	mov ecx, dword ptr [edx + 0x10]
	test ecx, 1
	jnz @no
	mov eax, dword ptr [edx + 0xC]
	cmp eax, dword ptr [0xB20EC0]
	jg @no
	jl @yes
	mov eax, dword ptr [edx + 0x8]
	cmp eax, dword ptr [0xB20EBC]
	jg @no
	@yes:
	xor eax, eax
	inc eax
	retn
	@no:
	xor eax, eax
	mov dword ptr []] .. MO.AttackSpcPtr .. [[], eax
	retn]])
	MO.CheckSpcAttack = check_spc_attack

	-- ecx - player_ptr
	-- edx - is_ranged (0 if melee)
	local set_spc_attack_cd = mem.asmproc([[
	push esi
	push edi
	push ebx
	mov esi, ecx
	mov ebx, edx
	mov edi, dword ptr []] .. MO.AttackSpcPtr .. [[]
	mov eax, dword ptr [edi + 0x14]
	test eax, eax
	jz @end
	test dword ptr [edi + 0x10], 0x10 ; CooldownReducedBySpeed
	jz @f1
	push eax
	mov ecx, esi
	mov edx, 5 ; Speed
	call absolute ]] .. MO.GetPlStatEffect .. [[;
	mov edx, eax
	pop eax
	sub eax, edx
	jns @f1 ; FIXME: compare with spc_minimal_cooldown
	xor eax, eax
	@f1:
	;mov ecx, eax
	;xor edx, edx ; round down because of recovery
	;call absolute ]] .. MO.Sec2Time .. [[;
	xor edx, edx
	add eax, dword ptr [0xB20EBC]
	adc edx, dword ptr [0xB20EC0]
	mov dword ptr [edi + 0x8], eax
	mov dword ptr [edi + 0xC], edx
	test dword ptr [edi + 0x10], 0x400 ; CooldownOther
	jz @end
	xor edx, edx
	inc edx
	sub edx, ebx
	mov ecx, esi
	call absolute ]] .. get_spc_attack_ptr .. [[;
	mov ebx, eax
	test dword ptr [edi + 0x10], 0x800 ; UseOtherCooldown
	jnz @other
	mov eax, dword ptr [edi + 0xC]
	mov edx, dword ptr [edi + 0x8]
	cmp eax, dword ptr [ebx + 0xC]
	jg @write1
	jl @end
	cmp edx, dword ptr [ebx + 0x8]
	jle @end
	jmp @write2
	@write1:
	mov dword ptr [ebx + 0xC], eax
	@write2:
	mov dword ptr [ebx + 0x8], edx
	jmp @end
	@other:
	mov eax, dword ptr [ebx + 0x14]
	test eax, eax
	jz @end
	test dword ptr [ebx + 0x10], 0x10
	jz @f2
	push eax
	mov ecx, esi
	mov edx, 5 ; Speed
	call absolute ]] .. MO.GetPlStatEffect .. [[;
	mov edx, eax
	pop eax
	sub eax, edx
	jns @f2 ; FIXME: compare with spc_minimal_cooldown
	xor eax, eax
	@f2:
	;mov ecx, eax
	;xor edx, edx
	;call absolute ]] .. MO.Sec2Time .. [[;
	xor edx, edx
	add eax, dword ptr [0xB20EBC]
	adc edx, dword ptr [0xB20EC0]
	cmp edx, dword ptr [ebx + 0xC]
	jl @end
	jg @write3
	cmp eax, dword ptr [ebx + 0x8]
	jle @end
	@write3:
	mov dword ptr [ebx + 0x8], eax
	mov dword ptr [ebx + 0xC], edx
	@end:
	pop ebx
	pop edi
	pop esi
	retn]])
	MO.SetSpcAttackCD = set_spc_attack_cd

	mem.asmpatch(0x42D884, [[
	mov dword ptr [ebp - 0x10], ebx
	]], 0x42D8A4 - 0x42D884)

	-- esi - player_ptr
	-- [ebp - 0xC] - target
	mem.asmpatch(0x42D93A, [[
	mov dword ptr []] .. MO.AttackDistance .. [[], ebx
	mov eax, dword ptr [ebp - 0xC]
	and eax, 7
	cmp al, 3
	jnz @ranged
	mov ecx, dword ptr [ebp - 0xC]
	sar ecx, 3
	call absolute ]] .. MO.GetMapMonsterPtr .. [[;
	mov ecx, eax
	call absolute ]] .. MO.GetDistanceToMonster .. [[;
	xor edx, edx
	cmp eax, 407
	jle @f1
	@ranged:
	mov edx, 1
	@f1:
	mov dword ptr []] .. MO.AttackDistance .. [[], eax
	mov dword ptr []] .. MO.AttackIsRanged .. [[], edx
	mov ecx, esi
	call absolute ]] .. check_spc_attack .. [[;
	mov edx, dword ptr [ebp - 0x10]
	cmp edx, 2 ; check for blaster
	jz @end
	mov dword ptr [ebp - 0x10], eax
	@end:
	mov eax, dword ptr [ebp - 0xC]
	and eax, 7
	]])

	mem.asmpatch(0x42DAB0, [[
	mov edx, dword ptr []] .. MO.AttackIsRanged .. [[]
	test edx, edx
	jz @melee
	push 0x4020
	jmp @f1
	@melee:
	push 0x2020
	@f1:
	mov ecx, esi
	call absolute ]] .. set_spc_attack_cd .. [[;
	mov eax, dword ptr []] .. MO.AttackSpcPtr .. [[]
	movzx eax, byte ptr [eax + 0x2]
	push eax
	mov ecx, esi
	call absolute 0x48EF4F ; GetSkill
	push eax
	mov ecx, dword ptr []] .. MO.AttackSpcPtr .. [[]
	movzx ecx, word ptr [ecx]
	mov edx, dword ptr [ebp - 0x8]
	;call absolute 0x425B67 ; put spell into spell queue
	jmp absolute 0x42DABA
	]])

	-- set spc attack recovery
	mem.asmpatch(0x42D3AB, [[
	test byte ptr [ebx + 0x9], 0x60
	jz @end
	mov ecx, dword ptr [ebp - 0x1C]
	mov esi, ecx
	xor edx, edx
	test byte ptr [ebx + 0x9], 0x40
	jz @f1
	inc edx
	@f1:
	call absolute ]] .. get_spc_attack_ptr .. [[;
	mov ecx, eax
	movzx eax, word ptr [ecx + 0x6]
	test dword ptr [ecx + 0x10], 0x10
	jz @f2
	push eax
	mov ecx, esi
	mov edx, 5 ; Speed
	call absolute ]] .. MO.GetPlStatEffect .. [[;
	mov edx, eax
	pop eax
	sub eax, edx
	@f2:
	xor esi, esi
	jmp absolute 0x42D3B3
	@end:
	xor esi, esi
	test byte ptr [ebx + 0x8], 0x20
	]])

	--[=[
	mem.asmpatch(0x42d884, "call absolute " .. GetCAttack)
	mem.asmpatch(0x42d889, "test eax, eax")
	mem.asmpatch(0x42d88c, "jnz 0x42d89d - 0x42d88c")

	mem.asmpatch(0x42d890, "call absolute " .. GetCAttack)
	mem.asmpatch(0x42d895, "test eax, eax")
	mem.asmpatch(0x42d89b, "je 0x42d8a4 - 0x42d89b")

	local CurAttack = mem.StaticAlloc(4)
	mem.u4[CurAttack] = -1

	mem.asmpatch(0x42dab0, [[
	mov ecx, esi
	mov eax, dword [ds:]] .. CurAttack .. [[]

	cmp eax, -1
	je @find

	cmp dword [ss:ebp-0x10], 0
	je @end2

	cmp eax, 0
	je @find
	jmp @end

	@end2:
	xor ecx, ecx
	jmp @exit

	@find:
	call absolute ]] .. GetCAttack .. [[;

	@end:
	mov dword [ds:]] .. CurAttack .. [[], -1
	mov ecx, eax

	@exit:
	]])

	local function GetDist(mon)
		local x, y, z  = XYZ(Party)
		local mx,my,mz = XYZ(mon)
		return sqrt((x-mx)^2 + (y-my)^2 + (z-mz)^2)
	end

	local BLASTER = const.Skills.Blaster
	local function HasBlaster(pl)
		local slot = pl.ItemMainHand
		local it = (slot ~= 0 and pl.Items[slot])
		return it and it.Condition:And(2) == 0 and Game.ItemsTxt[it.Number].Skill == BLASTER
	end

	local function SpecialAttackHook(d, pl, mon)
		local t = {Player = GetPlayer(pl), IsRanged = true, mon = GetMon(mon), Attack = mem.u4[CurAttack]}

		if HasBlaster(t.Player) then
			t.IsRanged =  true
			t.Attack = 135
		elseif t.mon then
			t.IsRanged = GetDist(t.mon) > 350
			t.Attack = Game.CharacterPortraits[t.Player.Face][t.IsRanged and "DefAttackR" or "DefAttackM"]
		else
			t.Attack = Game.CharacterPortraits[t.Player.Face].DefAttackR
		end
		events.call("GetSpecialAttack", t)

		local def = mem.u4[d.ebp-0x10]
		mem.u4[CurAttack] = t.Attack
		mem.u4[d.ebp-0x10] = t.Attack > 0 and def or 0
	end

	mem.autohook(0x42d91d, function(d) SpecialAttackHook(d, d.esi, d.ecx) end)
	mem.autohook2(0x42d966, function(d) SpecialAttackHook(d, d.esi, d.edi) end)
	]=]

	-- Remove SP requirements from custom attacks:

	mem.asmpatch(0x425b47, [[
	cmp dword [ss:ebp-0x24], 1
	je @fre
	mov dword [ds:ecx+0x1bfc], eax
	@fre:]])

	mem.asmpatch(0x425b1a, [[
	mov eax, dword [ds:ecx+0x1bfc]
	cmp dword [ss:ebp-0x24], 1
	je absolute 0x425b43]])

	mem.asmpatch(0x4262ce, [[
	mov edx, dword [ds:eax+0x1bfc]
	cmp dword [ss:ebp-0x24], 1
	je absolute 0x4262ec]])

	-- Load actual attack's sound:

	mem.asmpatch(0x42db69, [[
	mov ecx, dword [ss:esp-0xc]
	cmp ecx, 0x89
	je @std
	movsx ecx, word [ds:0x4fe128+ecx*2]
	push ecx
	jmp @end
	@std:
	push 0x46a0
	@end:]])

	-- Use race to define starting stats
	TmpTable = {0x4c6a17, 0x4c6a55, 0x4c6ab3, 0x4c6aea, 0x48fa84, 0x4903cf, 0x4903e6,
				0x4903fd, 0x490414, 0x49042b, 0x490442, 0x490459, 0x48f87d, 0x48f6eb}

	local NewCode = mem.asmproc([[
	movsx eax, byte [ds:ecx+0x353]
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. PortraitsTablePtr .. [[;
	movsx eax, byte [ds:eax+0x3f];
	retn]])

	for k,v in pairs(TmpTable) do
		mem.asmpatch(v, "call absolute " .. NewCode)
	end

end

local function ProcessDollTypesTable()

	local function GenerateDollTypesTable()

		local AvItemsPatterns	=	{"x	x	x	x	x	x	x",
									 "x	x	x	x	x	x	x",
									 "x	x	-	x	-	x	x",
									 "x	x	x	x	x	x	x",
									 "-	-	-	-	-	-	-"}

		local DollsTxtTable = io.open("Data/Tables/Character doll types.txt", "w")

		DollsTxtTable:write("#	Bow	Armor	Helm	Belt	Boots	Cloak	Weapon	RHo X	RHo Y	RHc X	RHc Y	RHf X	RHf Y	LHo X	LHo Y	LHc X	LHc Y	LHf X	LHf Y	OH Offset X	OH Offset Y	MH Offset X	MH Offset Y	Bow Offset X	Bow Offset Y	Shield X	Shield Y\n")

		for i = 0, 4 do

			local CurStr = i .. "\9" .. AvItemsPatterns[i+1] .. "\9"
							.. mem.i4[0x4f63d8+i*16] .. "\9" .. mem.i4[0x4f63dc+i*16] .. "\9"
							.. mem.i4[0x4f63e0+i*16] .. "\9" .. mem.i4[0x4f63e4+i*16] .. "\9"
							.. mem.i4[0x4f6398+i*8] .. "\9" .. mem.i4[0x4f639c+i*8] .. "\9"
							.. mem.i4[0x4f6338+i*8] .. "\9" .. mem.i4[0x4f633c+i*8] .. "\9"
							.. mem.i4[0x4f6378+i*8] .. "\9" .. mem.i4[0x4f637c+i*8] .. "\9"
							.. mem.i4[0x4f6358+i*8] .. "\9" .. mem.i4[0x4f635c+i*8] .. "\9"
							.. mem.i4[0x4f63b8+i*8] .. "\9" .. mem.i4[0x4f63bc+i*8] .. "\9"
							.. mem.i4[0x4f58a0+i*128] .. "\9" .. mem.i4[0x4f58a4+i*128] .. "\9"
							.. mem.i4[0x4f58a8+i*128] .. "\9" .. mem.i4[0x4f58ac+i*128] .. "\9"
							.. mem.i4[0x4f5898+i*128] .. "\9" .. mem.i4[0x4f589c+i*128] .. "\n"

			DollsTxtTable:write(CurStr)

		end

		io.close(DollsTxtTable)

	end


	local Counter, TypesCount, LineSize = 0, 0, 47
	local DollsTxtTable = io.open("Data/Tables/Character doll types.txt", "r")

	if not DollsTxtTable then
		GenerateDollTypesTable()
		DollsTxtTable = io.open("Data/Tables/Character doll types.txt", "r")
	end

	local LineIt = DollsTxtTable:lines()

	LineIt() -- Skip header
	for line in LineIt do
		TypesCount = TypesCount + 1
	end

	local DollsTablePtr = mem.StaticAlloc(TypesCount*LineSize)

	DollsTxtTable:seek("set")

	LineIt()
	for line in LineIt do
		local Words = string.split(line, "\9")

		for i = 0, 6 do
			if Words[i+2] == "x" then
				mem.u1[DollsTablePtr + Counter*LineSize + i] = 1
			end
		end

		for i = 0, 19 do
			mem.i2[DollsTablePtr + Counter*LineSize + 7 + i*2] = tonumber(Words[i+9])
		end

		Counter = Counter + 1
	end

	ChangeGameArray("CharacterDollTypes", DollsTablePtr, TypesCount)

	----

	local CheckItem = mem.asmproc([[
	movsx eax, byte [ds:ebx+0x353]; Portrait, ecx - item
	nop;memhook here
	nop
	nop
	nop
	nop
	retn]])

	local ItemTypes = {
		Bow 	= {3, 27},
		Weapon 	= {0,1,2,4,12,19,23,24,25,26,27,28,29,30,33,41},
		Armor 	= {3,20,30,31,32},
		Belt 	= {6,35},
		Helm 	= {5,34},
		Cloak 	= {7,36},
		Boots 	= {9,38}}

	local function CheckItemAvailability(TypeId, ItemId)

		local result = 1

		if TypeId > Game.CharacterDollTypes.count - 1 then
			return result
		end

		local EquipStat = Game.ItemsTxt[ItemId].EquipStat
		for k,v in pairs(ItemTypes) do
			if table.find(v, EquipStat) then
				result = Game.CharacterDollTypes[TypeId][k] and 1 or 0
				break
			end
		end

		if Game.CurrentPlayer >= 0 and Game.CurrentPlayer < Party.count then
			local t = {ItemId = ItemId, PlayerId = Game.CurrentPlayer, Available = result == 1}
			events.cocall("CanWearItem", t)
			result = t.Available and 1 or 0
		end

		return result
	end

	mem.hook(CheckItem + 7, function(d)
		d.eax = CheckItemAvailability(Game.CharacterPortraits[d.eax].DollType, d.ecx)
	end)

	mem.nop(0x49103a, 4)
 	mem.asmpatch(0x49103e, [[
	mov ecx, dword [ds:esi]
	call absolute ]] .. CheckItem)

	mem.nop(0x4674ec, 3)
 	mem.asmpatch(0x4674ef, [[call absolute ]] .. CheckItem)

	-- ? Check: 0x43b7e6 (0x4f63b8)

	-- . 0x4f5894 - y anchor point - static for all portraits
	-- + 0x4f5898, 0x4f589c + i*4 - offhand offsets???

	-- + 0x4f58a8, 0x4f58ac - bow offsets
	-- + 0x4f58a0, 0x4f58a4 - main hand offsets

	-- + 0x4f635c, 0x4f6358 - corrected in Items.lua

	-- 0x4f6418, 0x4f6430 - ??

	-- + 0x43a9e0 - setup pointer.

	-- + 0x4f5a98, 0x4f5a9c - corrected in Items.lua


	mem.asmpatch(0x43a9e0, [[
	mov eax, dword [ss:esp+0x38];]])

	-- Unk

	mem.asmpatch(0x43b83c, [[
	mov edx, eax
	shr edx, 7
	imul edx, edx, ]] .. LineSize .. [[;
	add edx, ]] .. DollsTablePtr .. [[;
	movsx edx, word [ds:edx+0x2b]
	cmp byte [ss:esp+0x28], bl;]])

	mem.asmpatch(0x43b875, [[
	mov ebx, eax
	shr ebx, 7
	imul ebx, ebx, ]] .. LineSize .. [[;
	add ebx, ]] .. DollsTablePtr .. [[;
	movsx ebx, word [ds:ebx+0x2d]
	add edx, ebx
	xor ebx, ebx;]])

	mem.asmpatch(0x43b888, [[
	shr eax, 7
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. DollsTablePtr .. [[;
	movsx eax, word [ds:eax+0x2d];]])

	-- Bow offsets

	mem.nop(0x43a510, 3)
	mem.asmpatch(0x43a513, [[
	imul ecx, ecx, ]] .. LineSize .. [[;
	add ecx, ]] .. DollsTablePtr .. [[;
	movsx edx, word [ds:ecx+0x27];]])

	mem.asmpatch(0x43a519, [[
	movsx ecx, word [ds:ecx+0x29];]])

	-- MH Offsets

	mem.asmpatch(0x43b5b2, [[
	mov ecx, dword [ss:esp+0x38]
	movsx ebx, word [ds:ecx+0x23]
	add edi, ebx
	xor ebx, ebx]])

	mem.asmpatch(0x43b5d4, [[
	movsx ebx, word [ds:ecx+0x25]
	add edi, ebx
	xor ebx, ebx]])

	-- LHc
	mem.asmpatch(0x43a99e, [[
	movsx ecx, word [ds:eax+0x19]
	movsx eax, word [ds:eax+0x17]
	jmp absolute 0x43a9aa]])

	mem.asmpatch(0x43ba97, [[
	mov eax, dword [ds:esp+0x24]
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. DollsTablePtr .. [[;
	movsx ecx, word [ds:eax+0x19]
	movsx eax, word [ds:eax+0x17]
	jmp absolute 0x43baa3]])

	-- LHo

	mem.nop(0x43a8e2, 3)
	mem.asmpatch(0x43a8e5, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. DollsTablePtr .. [[;
	movsx edx, word [ds:eax+0x15];]])

	mem.asmpatch(0x43a8f1, [[
	movsx ecx, word [ds:eax+0x13];]])
	-- pointer stored at 0x43a909

	mem.asmpatch(0x43b7ea, [[
	movsx eax, word [ds:ecx+0x13];]])

	mem.asmpatch(0x43b7f0, [[
	movsx ebx, word [ds:ecx+0x1f]
	add eax, ebx
	xor ebx, ebx]])

	mem.asmpatch(0x43b802, [[
	movsx eax, word [ds:ecx+0x15];]])

	mem.asmpatch(0x43b808, [[
	movsx ebx, word [ds:ecx+0x21]
	add eax, ebx
	xor ebx, ebx]])

	mem.asmpatch(0x43ba03, [[
	movsx ecx, word [ds:eax+0x13]
	movsx eax, word [ds:eax+0x15]
	jmp absolute 0x43ba0f]])

	mem.nop(0x4c4fee, 3)
	mem.asmpatch(0x4c4ff1, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. DollsTablePtr .. [[;
	movsx ecx, word [ds:eax+0x13]
	movsx eax, word [ds:eax+0x15]
	jmp absolute 0x4c4ffd]])

	-- LHf

	mem.asmpatch(0x43a952, [[
	movsx ecx, word [ds:eax+0x1d];]])

	mem.asmpatch(0x43a958, [[
	movsx eax, word [ds:eax+0x1b];]])

	mem.asmpatch(0x43ba68, [[
	movsx ecx, word [ds:eax+0x1d]
	movsx eax, word [ds:eax+0x1b]
	jmp absolute 0x43ba74]])

	-- RHo

	mem.nop(0x4c5061, 3)
	mem.asmpatch(0x4c5064, [[
	imul eax, eax, ]] .. LineSize .. [[;
	add eax, ]] .. DollsTablePtr .. [[;
	movsx ecx, word [ds:eax+0x7]
	movsx eax, word [ds:eax+0x9]
	jmp absolute 0x4c5070]])

	mem.asmpatch(0x43aa34, [[
	movsx ecx, word [ds:eax+0x9]
	movsx eax, word [ds:eax+0x7]
	jmp absolute 0x43aa40]])

	-- RHc

	mem.nop(0x43a9e4, 3)
	mem.asmpatch(0x43aa0d, [[
	movsx ecx, word [ds:eax+0xd]
	movsx eax, word [ds:eax+0xb]
	jmp absolute 0x43aa19]])

	mem.asmpatch(0x43b57c, [[
	movsx edi, word [ds:edi+0xb];]])

	mem.asmpatch(0x43b5c1, [[
	movsx edi, word [ds:edi+0xd];]])

	-- RHf

	mem.asmpatch(0x43b77b, [[
	movsx ecx, word [ds:eax+0x11];]])

	mem.asmpatch(0x43b787, [[
	movsx eax, word [ds:eax+0xf];]])


end


function events.GameInitialized1()

	ProcessVoicesTable()
	ProcessDollTypesTable()
	ProcessPortraitsTable()
	SetChooseCharHook()

end

MF.LogInit2(LogId)

