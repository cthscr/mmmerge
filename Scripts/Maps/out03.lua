-- Alvar
local MF = Merge.Functions

function events.AfterLoadMap()
	Party.QBits[803] = true	-- DDMapBuff
end

-- Town Portal fountain
evt.map[104] = function()
	Party.QBits[301] = true	-- TP Buff Alvar
	MF.SetLastFountain()
end
