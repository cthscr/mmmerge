-- Mount Nighon
local MF = Merge.Functions

function events.AfterLoadMap()
	Party.QBits[721] = true	-- TP Buff Nighon
	Party.QBits[825] = true	-- DDMapBuff
end

-- Town Portal fountain
evt.map[206] = function()
	MF.SetLastFountain()
end
