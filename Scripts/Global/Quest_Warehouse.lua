local LogId = "Quest_Warehouse"
local MF = Merge.Functions
MF.LogInit1(LogId)
local MM = Merge.ModSettings
local floor = math.floor
local strformat = string.format

local function get_date_dmy(time)
	local year = floor(time / const.Year) + Game.BaseYear
	local month = floor((time % const.Year) / const.Month) + 1
	local day = floor((time % const.Month) / const.Day) + 1
	return strformat("%02d.%02d.%d", day, month, year)
end

QuestNPC = 823	-- Kent Williams

Quest{
	Name = "Warehouse",
	Slot = 0,
	Branch = "",
	CanShow = function() return not vars.WarehouseBought end,
	Ungive = function()
		QuestBranch("init")
		Message(strformat(Game.NPCText[2834], MM.MM6WarehouseRent, MM.MM6WarehouseRent * 2))
			-- "Do you want to rent a warehouse? It will cost you %d gold and same amount per month. So now you have to pay %d. Agree?"
	end,
	Texts = {
		Topic = Game.NPCText[2833]	-- "Rent a warehouse"
	}
}

Quest{
	Name = "WarehouseInitY",
	Slot = 1,
	Branch = "init",
	Ungive = function()
		if Party.Gold >= MM.MM6WarehouseRent * 2 then
			Party.Gold = Party.Gold - MM.MM6WarehouseRent * 2
			vars.WarehouseTill = Game.Time + const.Month
			Message(strformat(Game.NPCText[2835], get_date_dmy(vars.WarehouseTill)))
				-- "You can use warehouse till %s."
		else
			Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
		end
		QuestBranch("")
	end,
	Texts = {
		Topic = Game.GlobalTxt[704]	-- "Yes"
	}
}

Quest{
	Name = "WarehouseN",
	Slot = 2,
	CanShow = function() return QuestBranch() ~= "" end,
	Ungive = function() QuestBranch("") end,
	Texts = {
		Topic = Game.GlobalTxt[705]	-- "No"
	}
}

Quest{
	Name = "WarehouseRent",
	Slot = 0,
	Branch = "",
	CanShow = function() return not vars.WarehouseBought and vars.WarehouseTill and vars.WarehouseTill > 0 end,
	Ungive = function()
		QuestBranch("rent")
		if vars.WarehouseTill < Game.Time then
			Message(strformat(Game.NPCText[2838], MM.MM6WarehouseRent))
				-- "You have a debt. Pay %d gold?"
		else
			Message(strformat(Game.NPCText[2837], MM.MM6WarehouseRent))
				-- "Pay monthly fee of %d gold?"
		end
	end,
	Texts = {
		Topic = Game.NPCText[2836]	-- "Pay the rent"
	}
}

Quest{
	Name = "WarehouseRentY",
	Slot = 1,
	Branch = "rent",
	Ungive = function()
		if Party.Gold >= MM.MM6WarehouseRent then
			Party.Gold = Party.Gold - MM.MM6WarehouseRent
			vars.WarehouseTill = vars.WarehouseTill + const.Month
			if vars.WarehouseTill < Game.Time then
				Message(Game.NPCText[2839])
					-- "You have a debt still. No usage permitted before you pay it out."
			else
				Message(strformat(Game.NPCText[2835], get_date_dmy(vars.WarehouseTill)))
					-- "You can use warehouse till %s."
			end
		else
			Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
		end
		QuestBranch("")
	end,
	Texts = {
		Topic = Game.GlobalTxt[704]	-- "Yes"
	}
}

Quest{
	Name = "WarehouseChest",
	Slot = 1,
	Branch = "",
	CanShow = function()
		return (vars.WarehouseBought or vars.WarehouseTill and vars.WarehouseTill > Game.Time)
			and (not vars.WarehouseChests or vars.WarehouseChests < 18)
	end,
	Ungive = function()
		if vars.WarehouseTill < Game.Time and not vars.WarehouseBought then
			Message(Game.NPCText[2842])
				-- "You have a debt. No operations before you pay it out."
		else
			QuestBranch("chest")
			Message(strformat(Game.NPCText[2841], MM.MM6WarehouseChest))
				-- "Do you want to buy and additional chest for %d gold?"
		end
	end,
	Texts = {
		Topic = Game.NPCText[2840]	-- "Buy a chest"
	}
}

Quest{
	Name = "WarehouseChestY",
	Slot = 1,
	Branch = "chest",
	Ungive = function()
		if Party.Gold >= MM.MM6WarehouseChest then
			Party.Gold = Party.Gold - MM.MM6WarehouseChest
			Message(Game.GlobalTxt[9])	-- "Congratulations!"
			vars.WarehouseChests = (vars.WarehouseChests or 0) + 1
		else
			Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
		end
		QuestBranch("")
	end,
	Texts = {
		Topic = Game.GlobalTxt[704]	-- "Yes"
	}
}

Quest{
	Name = "WarehouseBuy",
	Slot = 3,
	Branch = "",
	CanShow = function() return not vars.WarehouseBought and vars.WarehouseTill and vars.WarehouseTill > 0 end,
	Ungive = function()
		if vars.WarehouseTill < Game.Time then
			Message(Game.NPCText[2842])
				-- "You have a debt. No operations before you pay it out."
		else
			QuestBranch("buy")
			Message(strformat(Game.NPCText[2844], MM.MM6WarehouseCost))
				-- "Do you want to buy the warehouse for %d gold?"
		end
	end,
	Texts = {
		Topic = Game.NPCText[2843]	-- "Buy the warehouse"
	}
}

Quest{
	Name = "WarehouseBuyY",
	Slot = 1,
	Branch = "buy",
	Ungive = function()
		if Party.Gold >= MM.MM6WarehouseCost then
			Party.Gold = Party.Gold - MM.MM6WarehouseCost
			Message(Game.GlobalTxt[9])	-- "Congratulations!"
			vars.WarehouseBought = true
		else
			Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
		end
		QuestBranch("")
	end,
	Texts = {
		Topic = Game.GlobalTxt[704]	-- "Yes"
	}
}

MF.LogInit2(LogId)
