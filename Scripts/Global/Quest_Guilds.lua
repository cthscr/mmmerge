local LogId = "Quest_Guilds"
local MF = Merge.Functions
MF.LogInit1(LogId)
local MM, MT, MV = Merge.ModSettings, Merge.Tables, Merge.Vars
local strformat, strgsub = string.format, string.gsub

-- Guilds membership

local LastGuildJoinTopic = 0

local function generate_membership_quests(t)
	QuestNPC = t.QuestNPC
	t.Name = "Membership" .. t.Name
	t.Text2 = t.Text2 or 124
	Quest{
		Name = t.Name,
		Branch = "",
		Slot = t.Slot,
		Ungive = function()
			local member = false
			if t.QBit then
				if Party.QBits[t.QBit] then
					member = true
				end
			elseif t.Award then
				local pl = Party[MF.GetCurrentPlayer()]
				if pl.Awards[t.Award] then
					member = true
				end
			end
			if member then
				Message(Game.NPCText[t.Text2])
			else
				QuestBranchScreen("membership")
				Message(Game.NPCText[t.Text])
			end
		end,
		Texts = {
			Topic = Game.NPCTopic[t.Topic]
		}
	}
	Quest{
		Name = t.Name .. "_Y",
		Branch = "membership",
		Slot = 0,
		Ungive = function()
			local member = false
			local slot = MF.GetCurrentPlayer()
			if t.QBit then
				if Party.QBits[t.QBit] then
					member = true
				end
			elseif t.Award then
				local pl = Party[slot]
				if pl.Awards[t.Award] then
					member = true
				end
			end
			if member then
				Message(Game.NPCText[t.Text2])
			else
				if Party.Gold >= t.Cost then
					Party.Gold = Party.Gold - t.Cost
					if t.QBit then
						if t.Award then
							Party.QBits[t.QBit] = true
							for k, _ in Party do
								MF.AwardAdd(t.Award, k)
							end
						else
							MF.QBitAdd(t.QBit)
						end
					elseif t.Award then
						MF.AwardAdd(t.Award, slot)
					end
					if t.ABit then
						Party.AutonotesBits[t.ABit] = true
					end
				else
					Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
				end
			end
			ExitQuestBranch()
		end,
		Texts = {
			Topic = Game.GlobalTxt[704]	-- "Yes"
		}
	}
	Quest{
		Name = t.Name .. "_N",
		Branch = "membership",
		Slot = 1,
		Ungive = function() ExitQuestBranch() end,
		Texts = {
			Topic = Game.GlobalTxt[705]	-- "No"
		}
	}
	Quest{
		Name = t.Name .. "_2",
		Branch = "membership",
		Slot = 2
	}
	Quest{
		Name = t.Name .. "_3",
		Branch = "membership",
		Slot = 3
	}
	Quest{
		Name = t.Name .. "_4",
		Branch = "membership",
		Slot = 4
	}
end

function events.ExitNPC()
	LastGuildJoinTopic = 0
end

local function JoinGuild(GuildType, Cost, Text, ABit, TopicId, QBit, Award)

	if LastGuildJoinTopic ~= TopicId then
		LastGuildJoinTopic = TopicId
		Message(Game.NPCText[Text])
		return
	end

	--vars.GuildMembership = vars.GuildMembership or {}

	--if vars.GuildMembership[GuildType] then
	if Party.QBits[QBit] then
		Message(Game.NPCText[124])	-- Already member of this guild.
	else
		if Party.Gold >= Cost then
			evt.Subtract("Gold", Cost)
			evt.Add("AutonotesBits", ABit)
			Party.QBits[QBit] = true
			if Award then
				evt.All.Add("Awards", Award)
			end
			--vars.GuildMembership[GuildType] = true
			Message(Game.NPCText[Text])
		else
			Message(Game.GlobalTxt[155]) -- Not enough gold.
		end
	end

	FirstClick = false

end

-- Antagarich

evt.Global[1150] = function(i) JoinGuild(14, 100, 1830, 564, i, 1596, 160) end	-- Elements
evt.Global[1151] = function(i) JoinGuild(15, 100, 1039, 565, i, 1597, 161) end	-- Self
evt.Global[1152] = function(i) JoinGuild(6, 50, 1040, 566, i, 1598, 162) end		-- Air
evt.Global[1153] = function(i) JoinGuild(8, 50, 1041, 567, i, 1599, 163) end		-- Earth
evt.Global[1154] = function(i) JoinGuild(5, 50, 1042, 568, i, 1600, 164) end		-- Fire
evt.Global[1155] = function(i) JoinGuild(7, 50, 1043, 569, i, 1601, 165) end		-- Water
evt.Global[1156] = function(i) JoinGuild(11, 50, 1044, 570, i, 1602, 166) end		-- Body
evt.Global[1157] = function(i) JoinGuild(10, 50, 1045, 571, i, 1603, 167) end		-- Mind
evt.Global[1158] = function(i) JoinGuild(9, 50, 1046, 572, i, 1604, 168) end		-- Spirit
evt.Global[1159] = function(i) JoinGuild(12, 1000, 1047, 573, i, 1605, 169) end	-- Light
evt.Global[1160] = function(i) JoinGuild(13, 1000, 1048, 574, i, 1606, 170) end	-- Dark

-- Enroth

evt.Global[1694] = function(i) JoinGuild(14, 100, 1830, 627, i, 1659, 221) end	-- Same order
evt.Global[1695] = function(i) JoinGuild(15, 100, 1039, 628, i, 1660, 222) end
evt.Global[1702] = function(i) JoinGuild(6, 50, 1040, 635, i, 1667, 229) end
evt.Global[1703] = function(i) JoinGuild(8, 50, 1041, 636, i, 1668, 230) end
evt.Global[1704] = function(i) JoinGuild(5, 50, 1042, 637, i, 1669, 231) end
evt.Global[1705] = function(i) JoinGuild(7, 50, 1043, 638, i, 1670, 232) end
evt.Global[1706] = function(i) JoinGuild(11, 50, 1044, 639, i, 1671, 233) end
evt.Global[1707] = function(i) JoinGuild(10, 50, 1045, 640, i, 1672, 234) end
evt.Global[1708] = function(i) JoinGuild(9, 50, 1046, 641, i, 1673, 235) end
evt.Global[1709] = function(i) JoinGuild(12, 1000, 1047, 642, i, 1674, 236) end
evt.Global[1710] = function(i) JoinGuild(13, 1000, 1048, 643, i, 1675, 237) end

----------------------------------------
-- Mercenary guilds

-- Buccaneer's Lair
-- Arthur O'Leery (1257, OutE2)
generate_membership_quests({QuestNPC = 981, Name = "EnrothGuildBuccaneersLair1", Slot = 2,
	QBit = 1661, Topic = 1696, Text = 1832, Text2 = 1829, Award = 223, ABit = 629, Cost = 25})
-- Hejaz Mawsil (1328, OutE3)
generate_membership_quests({QuestNPC = 1073, Name = "EnrothGuildBuccaneersLair2", Slot = 2,
	QBit = 1661, Topic = 1696, Text = 1832, Text2 = 1829, Award = 223, ABit = 629, Cost = 25})

-- Protection Services
-- Lucky Oster (1298, OutD1)
generate_membership_quests({QuestNPC = 844, Name = "EnrothGuildProtectionServices1", Slot = 2,
	QBit = 1662, Topic = 1697, Text = 1833, Text2 = 1829, Award = 224, ABit = 630, Cost = 50})
-- Marcellus Lutvig (1220, OutC1)
generate_membership_quests({QuestNPC = 846, Name = "EnrothGuildProtectionServices2", Slot = 2,
	QBit = 1662, Topic = 1697, Text = 1833, Text2 = 1829, Award = 224, ABit = 630, Cost = 50})

-- Smuggler's Guild
-- Tess Tucker (1397, OutB2)
generate_membership_quests({QuestNPC = 835, Name = "EnrothGuildSmugglersGuild1", Slot = 2,
	QBit = 1663, Topic = 1698, Text = 1834, Text2 = 1829, Award = 225, ABit = 631, Cost = 50})
-- Sergio Carrington (1266, OutC2)
generate_membership_quests({QuestNPC = 849, Name = "EnrothGuildSmugglersGuild2", Slot = 2,
	QBit = 1663, Topic = 1698, Text = 1834, Text2 = 1829, Award = 225, ABit = 631, Cost = 50})

-- Blade's End
-- Harold Hess (1339, OutE3)
generate_membership_quests({QuestNPC = 818, Name = "EnrothGuildBladesEnd1", Slot = 2,
	QBit = 1664, Topic = 1699, Text = 1835, Text2 = 1829, Award = 226, ABit = 632, Cost = 25})
-- Fredrick Piles (1308, OutC1)
generate_membership_quests({QuestNPC = 821, Name = "EnrothGuildBladesEnd2", Slot = 2,
	QBit = 1664, Topic = 1699, Text = 1835, Text2 = 1829, Award = 226, ABit = 632, Cost = 25})

-- Duelist's Edge
-- Gonzalo Ramirez (1315, OutE2)
generate_membership_quests({QuestNPC = 845, Name = "EnrothGuildDuelistsEdge1", Slot = 2,
	QBit = 1665, Topic = 1700, Text = 1836, Text2 = 1829, Award = 227, ABit = 633, Cost = 50})
-- Winston Historian (1281, OutC2)
generate_membership_quests({QuestNPC = 860, Name = "EnrothGuildDuelistsEdge2", Slot = 2,
	QBit = 1665, Topic = 1700, Text = 1836, Text2 = 1829, Award = 227, ABit = 633, Cost = 50})

-- Berserker's Fury
-- Horace Rose (1283, OutD1)
generate_membership_quests({QuestNPC = 838, Name = "EnrothGuildBerserkersFury1", Slot = 2,
	QBit = 1666, Topic = 1701, Text = 1837, Text2 = 1829, Award = 228, ABit = 634, Cost = 50})
-- Andrew Besper (1348, OutD3)
generate_membership_quests({QuestNPC = 862, Name = "EnrothGuildBerserkersFury2", Slot = 2,
	QBit = 1666, Topic = 1701, Text = 1837, Text2 = 1829, Award = 228, ABit = 634, Cost = 50})

-------------------------
-- Guilds of Inner Power

for npc, amastery in pairs(MT.GuildNPCMastery) do
	local award = MT.GuildNPCAward[npc]

	Quest{
		Name = "InnerPower"..npc.."_Learn",
		NPC = npc,
		Slot = 3,
		Branch = "",
		Ungive = function()
			local pl = Party[MF.GetCurrentPlayer()]
			if pl.Awards[award] then
				QuestBranchScreen("learn")
				--Message(strgsub(Game.GlobalTxt[401], "%%lu", MM.AbilitySkillCost))
			else
				Message(Game.NPCText[122])	-- "You must be a member of this guild to study here."
			end
		end,
		Texts = {
			Topic = Game.GlobalTxt[160]	-- "Learn Skills"
		}
	}

	for askill = 21, 23 do
		Quest{
			Name = "InnerPower"..npc.."_Ability"..askill,
			NPC = npc,
			Slot = askill - 21,
			Branch = "",
			Ungive = function()
				local pl = Party[MF.GetCurrentPlayer()]
				if pl.Awards[award] then
					QuestBranchScreen("ability"..askill)
				else
					Message(Game.NPCText[122])	-- "You must be a member of this guild to study here."
				end
			end,
			Texts = {
				Topic = Game.SkillNames[askill].." Magic"
			}
		}

		local aspells = MF.GetAbilitySpells(askill, amastery)
		if aspells then
			for k, v in pairs(aspells) do
				Quest{
					Name = "InnerPower"..npc.."_Spell"..v,
					NPC = npc,
					Slot = k - 1,
					Branch = "ability"..askill,
					CanShow = function()
						local pl = Party[MF.GetCurrentPlayer()]
						local res
						if not pl.Spells[v] then
							local rspells = MF.GetRaceAbilitySpells(pl.Attrs.Race, askill, 4, true)
							res = rspells and table.find(rspells, v)
						end
						return res and true or false
					end,
					Ungive = function()
						local pl = Party[MF.GetCurrentPlayer()]
						if pl.Spells[v] then
							Message(strformat(Game.GlobalTxt[380], Game.SpellsTxt[v].Name))
								-- "You already know the %s spell"
							return
						end
						local mastery = MF.GetPlayerSkillMastery(pl, askill)
						local rspells = MF.GetRaceAbilitySpells(pl.Attrs.Race, askill, mastery, true)
						local res = rspells and table.find(rspells, v)
						if res then
							if Party.Gold >= MM["AbilitySpellCost"..amastery] then
								Party.Gold = Party.Gold - MM["AbilitySpellCost"..amastery]
								pl.Spells[v] = true
								pl:ShowFaceAnimation(21)
								--ExitQuestBranch()
							else
								pl:ShowFaceAnimation(38)
								Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
							end
						else
							pl:ShowFaceAnimation(20)
							Message(strformat(Game.GlobalTxt[381], Game.SpellsTxt[v].Name))
								-- "You don't have the skill to learn %s"
						end
					end,
					Texts = {
						Topic = Game.SpellsTxt[v].Name
					}
				}
			end
		end
		Quest{
			Name = "InnerPower"..npc.."_Skill"..askill,
			NPC = npc,
			Slot = askill - 21,
			Branch = "learn",
			CanShow = function()
				local pl = Party[MF.GetCurrentPlayer()]
				local res
				if pl.Skills[askill] > 0 then
					res = 0
				else
					res = GetMaxSkillLevel(pl.Attrs.Race, pl.Class, askill, pl.Attrs.Maturity)
				end
				return res > 0
			end,
			Ungive = function()
				local pl = Party[MF.GetCurrentPlayer()]
				if Party.Gold >= MM.AbilitySkillCost then
					if pl.Skills[askill] == 0 then
						Party.Gold = Party.Gold - MM.AbilitySkillCost
						pl.Skills[askill] = true
						pl:ShowFaceAnimation(78)
					end
				else
					pl:ShowFaceAnimation(38)
					Message(Game.GlobalTxt[155])	-- "You don't have enough gold"
				end
				--ExitQuestBranch()
			end,
			Texts = {
				Topic = Game.SkillNames[askill]
			}
		}
	end
end

MF.LogInit2(LogId)
