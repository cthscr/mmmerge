cthscr's Merge Revamp project

Splitted off from Community Branch (master branch) to be an independent
project aiming to fulfil my personal preferences. And to have a better name also.

## Built on top of MMMerge Pack 2023-11-05! Using previous packs will probably cause various issues.

### Installation instructions

https://gitlab.com/cthscr/mmmerge/-/wikis/Manual/Install

### Revamp-specific features

https://gitlab.com/cthscr/mmmerge/-/wikis/Revamp/Features

### Issue Tracker

https://gitlab.com/cthscr/mmmerge/-/issues

### Links

[Base] Merge thread: https://www.celestialheavens.com/forum/10/16657

Community Branch repo: ~~https://gitlab.com/templayer/mmmerge/~~ (dead)

Merge Issue Tracker: ~~https://gitlab.com/templayer/mmmerge/-/issues~~ (restricted access)

Rodril's Issue Tracker: https://gitlab.com/letr.rod/mmmerge/-/issues
